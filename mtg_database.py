import copy
import json
import logging
import os
import random
import requests
import sqlalchemy
import statistics
import time

from copy import deepcopy
from requests.exceptions import ConnectionError
from sqlalchemy import MetaData, Table, Column, Integer, String, Boolean
from sqlalchemy import create_engine, delete, inspect, insert, select, update

#OBJECTS
# ----SQL database----
class Deck_DB:

    def __init__(self, engine):
        
        self.engine = engine
        self.metadata_obj = MetaData()

    def refresh_meta(self):
    #refreshes the metadata representation of the database
        self.metadata_obj.clear()
        self.metadata_obj.reflect(self.engine)

    def add_deck(self, deck_name):
    #adds a deck to the database
#FUTURE NOTE: nullable is currently set to true which is not ideal - set it this way due to how insert statements are constructed

        table_obj = Table(
                deck_name,
                self.metadata_obj,
                Column('Name', String, nullable = True),
                Column('Type', String),
                Column('Subtype', String),
                Column('CMC', Integer),
                Column('CC', Integer),
                Column('MR', Boolean))
        table_obj.create(self.engine)

    def rm_deck(self, deck_name):
    #removes a deck from the database

        self.deck_name = deck_name
        self.refresh_meta()
        table_obj = Table(self.deck_name, self.metadata_obj)
        table_obj.drop(self.engine)

    def fetch_decklist(self):
    #returns current decklist as a list
        
        self.refresh_meta()
        decklist = []
        for n in self.metadata_obj.tables.keys():
            decklist.append(n)
        return decklist

    def validate_deck(self, deck_name):
    #validates whether or not a deck name exists and meets the name criterion
        
        self.refresh_meta()
        decklist = self.metadata_obj.tables.keys()
        char_limit = 30

        #duplicate name validation
        for n in decklist:
            if n == deck_name:
                print('Error: deck name must be a unique name')
                return False
   
        #name length validation
        if len(deck_name) > char_limit: 
            print(f'Error: deck name must be less than {char_limit} characters long')
            return False
        elif len(deck_name) <= char_limit:
            return True   

class Deck_Table:
#FUTURE NOTE: add option to retain state of deck to simulate changes before commiting

    def __init__(self, engine, deck_name):

        self.engine = engine
        self.deck_name = deck_name
        self.metadata_obj = MetaData()
        self.table_obj = Table(self.deck_name, self.metadata_obj, autoload_with = self.engine) 
        self.deck_validity = False

    def refresh_meta(self):
    #refreshes the metadata representation of the database
        
        self.metadata_obj.clear()
        self.metadata_obj.reflect(self.engine)

    def add_card(self, card_data):
#FUTURE NOTE: add a method that enables user to assign a commander
#FUTURE NOTE: create solution for adding basic lands in bulk
#FUTURE NOTE: need to add exception handling and testing for repeated primary key entries

        parsed_card = json.loads(card_data.text)
        type_subtype = self.convert_type_subtype(parsed_card['type_line'])

        self.refresh_meta()        
        stmnt = insert(self.table_obj).values(
            Name = parsed_card['name'],
            Type = type_subtype[0],
            Subtype = type_subtype[1],
            CMC = parsed_card['cmc'],
            CC = self.convert_mana_cost(parsed_card['mana_cost']),
            MR = False
            )
        stmnt.compile()

        with self.engine.connect() as conn:
            conn.execute(stmnt)
            conn.commit()

    def rm_card(self, card_name):
    #remove row from table
#FUTURE NOTE: when implementing this in Flask rm cards via mouse selection
        
        self.refresh_meta()
        stmnt = delete(self.table_obj).where(self.table_obj.c.Name == card_name)
        stmnt.compile()

        with self.engine.connect() as conn:
            conn.execute(stmnt)
            conn.commit()

    def replace_card(self, rm_card_name, new_card_data):
    #replaces one card with a another - just a composite method

        self.refresh_meta()
        self.rm_card(rm_card_name)
        self.add_card(new_card_data)

    def convert_mana_cost(self, mana_cost):
    #converts mana cost from scryfall to something more useful

#NOTE: OR costs are handled by giving one count to each
#FUTURE NOTE: need to add colorless mana costs
        color_count = ''
        color_ordering = ['B','U','G','P','R']
        for c in color_ordering:
            color_count += (str(mana_cost.count(c)) + ',')
        color_count = color_count[:-1]
        return color_count

    def convert_type_subtype(self, string):
    #splits a single string consisting of type and subtype attributes into two strings

#BUG NOTE: testing this will fail if there are more than two items in the split list
        type_subtype = string.replace(u' \u2014 ', '$').split('$')
        if len(type_subtype) < 2:
            type_subtype.append('None')
        return type_subtype

    def identify_rock(self, card_name):
    #positively identifies a user-selected card as a mana rock in the database
#FUTURE NOTE: consider how to remove MR designator if player changes their mind (or was wrong)
        
        self.refresh_meta()
        stmnt = select(self.table_obj).where(self.table_obj.c.Name == card_name)
        stmnt.compile()

        with self.engine.connect() as conn:

            for n in conn.execute(stmnt):

                print(n.Type)
                if n.Type == 'Artifact':
                    stmnt = update(self.table_obj).where(self.table_obj.c.Name == card_name).values(MR = True)
                    stmnt.compile()
                    conn.execute(stmnt)
                    conn.commit()
                    return True

    def select_where(self, type_select):
    #this is a helper function that returns all cards that match the user-requested Type

        self.refresh_meta()
        stmnt = select(self.table_obj).where(self.table_obj.c.Type == type_select)
        stmnt.compile()

        card_list = []
        with self.engine.connect() as conn:
            for n in conn.execute(stmnt):
                card_list.append(n.Name)

        return card_list

    def print_rocks(self):
#FUTURE NOTE: consider if kwargs can be used to combine this method with the print_cardlist() method
#FUTURE NOTE: refactor to be similar to the 'fetch' methods

        self.refresh_meta()
        stmnt = select(self.table_obj).where(self.table_obj.c.MR == True)
        stmnt.compile()

        with self.engine.connect() as conn:

            for n in conn.execute(stmnt):
                print(n.Name)

    def fetch_card_names(self):

        self.refresh_meta()
        stmnt = select(self.table_obj).order_by(self.table_obj.c.Type, self.table_obj.c.Name)
        stmnt.compile()
        card_list = []

        with self.engine.connect() as conn:
            for n in conn.execute(stmnt):
                print_line = n.Name
                card_list.append(print_line)
        
        return card_list  

    def fetch_card_data(self):
    #prints cardlist of the deck
#FUTURE NOTE: need to add ORDER BY option (default set to type and CMC)

        self.refresh_meta()
        stmnt = select(self.table_obj).order_by(self.table_obj.c.Type, self.table_obj.c.Name)
        stmnt.compile()
        card_list = []

        with self.engine.connect() as conn:
            for n in conn.execute(stmnt):
                print_line = (
                    self.pad_string(n.Name, 35) +
                    self.pad_string(n.Type, 25) +
                    self.pad_string(n.Subtype, 15) +
                    self.pad_string(str(n.CMC), 5) +
                    self.pad_string(n.CC, 10) +
                    self.pad_string(str(n.MR), 5)
                    )
                card_list.append(print_line)
        
        return card_list

    def print_header(self):
#FUTURE NOTE: refactor this because the current implementation is dumb - solution should be easier to implement in Flask
        pad = [35, 25, 15, 5, 10, 5]
        col_header = ''

        print('\nCC displays in following order: B U G P R\n')

        i = 0
        for col in self.table_obj.c.keys():
            col_header += self.pad_string(col, pad[i])
            i += 1
        print(col_header)
        print('-'*len(col_header))

    def pad_string(self, input_str, pad_lim):
#FUTURE NOTE: determine a better way to implement this method (and others like it), like a helper function?

        str_length = len(str(input_str))
        if str_length < pad_lim:
            pad = pad_lim - str_length
            input_str = str(input_str) + pad * ' '

        return input_str

    #validate card exists
    def validate_card_api(self, card_name):

        time.sleep(1)

        payload = {'exact':card_name}
        
        try:
            r = requests.get('https://api.scryfall.com/cards/named', params = payload)
        except ConnectionError:
            print('Error: not connected to internet, try again')
            return False

        parsed = json.loads(r.text)
        
        if parsed['object'] == 'error':
            print('Error: card not recognized, try again')
            return False
        elif parsed['object'] == 'card':
            return r

    def validate_card_table(self, card_name):

        self.metadata_obj.clear()
        self.metadata_obj.reflect(self.engine)

        stmnt = select(self.table_obj).where(self.table_obj.c.Name == card_name)
        stmnt.compile()

        with self.engine.connect() as conn:
            conn.execute(stmnt)
            conn.commit()

    def validate_card_count(self):
    #validate size (99 cards, command is excluded from actual decklist)

        self.metadata_obj.clear()
        self.metadata_obj.reflect(self.engine)

        stmnt = select(self.table_obj)
        stmnt.compile()

        with self.engine.connect() as conn:

            count = conn.execute(stmnt)
            count = len(count.scalars().all())

        if count < 99:
            print(f'Deck has {count} cards, add cards until deck is valid')
            self.deck_validity = False
            return False

        elif count > 99:
            print(f'Deck deck has {count} cards, remove cards until deck is valid')
            self.deck_validity = False
            return False

        elif count == 99:
            print('Deck is valid! Remove or replace cards instead of adding')
            self.deck_validity = True
            return True

class Deck_Obj:
    
    def __init__(self, deck_inst):
        
        self.deck_inst = deck_inst
        self.engine = deck_inst.engine
        self.decklist = []
        self.nonempty = True

        stmnt = select(self.deck_inst.table_obj)
        stmnt.compile()

        with self.engine.connect() as conn:
            for n in conn.execute(stmnt):
                
                card_inst = Card_Obj(n)
                self.decklist.append(card_inst)

    def shuffle_deck(self):
        random.shuffle(self.decklist)

    def draw_one(self):
        
        try:
            return self.decklist.pop()
        except IndexError:
            print('Error: deck is empty')
            self.nonempty = False
            return False

class Card_Obj:
    
    def __init__(self, n):

        self.n = n
        self.card_name = n.Name
        self.card_type = n.Type
        self.card_subtype = n.Subtype
        self.card_cmc = n.CMC
        self.card_cc = n.CC
        self.card_mr = n.MR

class Player_Obj:

    def __init__(self):

        self.hand = []

    def draw_one(self, card_obj):

        self.hand.append(card_obj)
        logging.info(f'Drew card: {card_obj.card_name} (Type: {card_obj.card_type}, CMC: {card_obj.card_cmc})')

class Sim_Obj:

    def __init__(self):

        self.lands_played = 0
        self.mr_played = 0
        
        self.perm_count = 0
        self.perm_sum = 0
        self.nonperm_count = 0
        self.nonperm_sum = 0
        
        self.landbase = {}
        self.mr_base = {}
        self.perm_mean = {}
        self.nonperm_mean = {}

#FUNCTIONS
#----validations & conversions----
def menu_selector(menu_options):
#this function displays a basic menu and enables the user to input a valid selection
#menu_options must be a list of strings or dictionary 

    i = 1
    menu_dict = {}
    for n in menu_options:
        print(f'{str(i)}.) {n}')
        menu_dict.update({i : n})
        i += 1

    validation = False
    while validation == False:
        
        try:
            user_selection = int(input('Select an option from above: '))
            
            if user_selection in menu_dict:
                validation = True
                return user_selection
            else:
                print('Error: selection out of range, try again')
                validation = False

        except ValueError:
            print('Error: selection not an integer, try again')
            validation = False

def int_validator(user_input):

        try:
            user_input = int(user_input)
            validation = True
            return user_input

        except ValueError:
            print('Error: selection not an integer, try again')
            return False

def convert_type_subtype(bulk_string):
#splits a single string consisting of type and subtype attributes into two strings
#BUG NOTE: testing this will fail if there are more than two items in the split list
        type_subtype = bulk_string.replace(u' \u2014 ', '$').split('$')
        if len(type_subtype) < 2:
            type_subtype.append('None')
        return type_subtype

#----playtest----
def playtest(engine, deck_name, sim_turns, sim_iterations):
#this function simulates a simplified game of MTG
#every turn, if able, the player plays a land, then a mana rock if they can afford it
#non-permanents and other permanents are never played - rather, their average cost for the turn is reported

#INIT
    logging.info(f'###STARTING SIMULATION###')
    ref_deck = Deck_Table(engine, deck_name)
    sim_data = []
    
#SIMULATION LOGIC
    i = 1
    while i <= sim_iterations:

    #OBJECT INIT
        player_inst = Player_Obj()
        sim_inst = Sim_Obj()
        sim_deck = Deck_Obj(ref_deck)
        sim_deck.shuffle_deck()

    #OPENING HAND
        logging.info(f'---SIM ITERATION {i}---')
        logging.info('Drawing opening hand')
        j = 1
        while j <= 7:

            popped_card = sim_deck.draw_one()
            player_inst.draw_one(popped_card)
            j += 1
    
        #PERM / NON-PERM LOGIC
            if (
                popped_card.card_type == 'Instant' or 
                popped_card.card_type == 'Sorcery'):

                sim_inst.nonperm_sum += popped_card.card_cmc
                sim_inst.nonperm_count += 1
                continue

            elif (
                popped_card.card_type != 'Instance' and 
                popped_card.card_type != 'Sorcery' and 
                popped_card.card_type != 'Land' and 
                popped_card.card_type != 'Basic Land'):

                sim_inst.perm_sum += popped_card.card_cmc
                sim_inst.perm_count += 1
                continue

    #TURN LOGIC
        j = 1
        while j <= sim_turns and sim_deck.nonempty == True:
                
            logging.info(f'{len(sim_deck.decklist)} cards in the deck')
            logging.info(f'-Playing turn {j}')
            popped_card = sim_deck.draw_one()

            #False is an indicator that the deck is empty
            if popped_card == False:
                continue

            player_inst.draw_one(popped_card)

        #PERM / NON-PERM LOGIC
            if (
                popped_card.card_type == 'Instant' or 
                popped_card.card_type == 'Sorcery'):
                
                sim_inst.nonperm_sum += popped_card.card_cmc
                sim_inst.nonperm_count += 1

            elif (
                popped_card.card_type != 'Instance' and 
                popped_card.card_type != 'Sorcery' and 
                popped_card.card_type != 'Land' and 
                popped_card.card_type != 'Basic Land' and
                popped_card.card_type != 'Legendary Land'):
                
                sim_inst.perm_sum += popped_card.card_cmc
                sim_inst.perm_count += 1
            
            try:
                sim_inst.nonperm_mean.update({j : sim_inst.nonperm_sum / sim_inst.nonperm_count})
                logging.info(f'Total non-permanent cost is: {sim_inst.nonperm_sum}')
                logging.info(f'Total non-permanent count is: {sim_inst.nonperm_count}')
                logging.info(f'Average non-permanent cost is now: {sim_inst.nonperm_mean[j]}')
                
            except ZeroDivisionError:
                sim_inst.nonperm_mean.update({j : 0})
                logging.info(f'No non-permanents in hand')

            try:
                sim_inst.perm_mean.update({j : sim_inst.perm_sum / sim_inst.perm_count})
                logging.info(f'Total non-land permanent cost is: {sim_inst.perm_sum}')
                logging.info(f'Total non-land permanent count is: {sim_inst.perm_count}')
                logging.info(f'Average non-land permanent cost is now: {sim_inst.perm_mean[j]}')
        
            except ZeroDivisionError:
                sim_inst.perm_mean.update({j : 0})
                logging.info(f'No permanents in hand')
        
        #LAND LOGIC  
            k = 0
            land_played = False
            for n in player_inst.hand:
                if (n.card_type == 'Basic Land' or n.card_type == 'Land') and land_played == False:

                    played_card = player_inst.hand.pop(k)
                    logging.info(f'Played {played_card.card_name}')
                    
                    sim_inst.lands_played += 1
                    logging.info(f'{sim_inst.lands_played} total lands played')
                    
                    land_played = True
                k += 1

            sim_inst.landbase.update({j : sim_inst.lands_played})
            logging.info(f'{len(player_inst.hand)} cards now in hand')
            
        #MANA ROCK LOGIC
#FUTURE NOTE: implement a solution that 'taps' lands
            k = 0
            for n in player_inst.hand:
                if (n.card_mr == True and sim_inst.lands_played >= n.card_cmc):

                    played_card = player_inst.hand.pop(k)
                    logging.info(f'Played {played_card.card_name}')
                    
                    sim_inst.mr_played += 1
                    logging.info(f'{sim_inst.mr_played} total mana rocks played')
                    break

                k += 1
            
            sim_inst.mr_base.update({j : sim_inst.mr_played})
            logging.info(f'{len(player_inst.hand)} cards now in hand')

            j += 1

        sim_data.append(sim_inst)
        i += 1

    print(f'{sim_iterations} simulation iterations performed')

    #sim_data is a list of dictionaries
    #the dictionary keys are the turn number; the values are simulation objects
    return sim_data

def calc_medians(sim_data):

    landbase_data = []
    mr_data = []
    perm_data = []
    nonperm_data = []
    
    i = 1
    while i <= len(sim_data[0].landbase):
        
        data_sublist = []
        for n in sim_data:  
            data_sublist.append(n.landbase[i])
        landbase_data.append(data_sublist)

        data_sublist = []
        for n in sim_data:  
            data_sublist.append(n.mr_base[i])
        mr_data.append(data_sublist)
        
        data_sublist = []
        for n in sim_data:  
            data_sublist.append(n.perm_mean[i])
        perm_data.append(data_sublist)

        data_sublist = []
        for n in sim_data:  
            data_sublist.append(n.nonperm_mean[i])
        nonperm_data.append(data_sublist)

        i += 1

    print('--RESULTS--')
    print(
        pad_string('TURN', 10) +
        pad_string('LANDBASE', 10) +
        pad_string('MANA ROCKS', 12) +
        pad_string('PERM AVG', 12) +
        pad_string('NONPERM AVG', 12)
        )
    
    i = 1
    while i <= len(landbase_data):

        print(
            pad_string(str(i), 10) +
            pad_string(str(statistics.median(landbase_data[i - 1])), 10) +
            pad_string(str(statistics.median(mr_data[i - 1])), 12) +
            pad_string(str(round(statistics.median(perm_data[i - 1]), 1)), 12) +
            pad_string(str(round(statistics.median(nonperm_data[i - 1]), 1)), 12)
            )
        i += 1

def pad_string(input_str, pad_lim):

        str_length = len(str(input_str))
        if str_length < pad_lim:
            pad = pad_lim - str_length
            input_str = str(input_str) + pad * ' '

        return input_str

#----MAIN LOGIC----
#FUTURE NOTE: restrict options based on color identity and format legality
if __name__ == '__main__':
    
    os.system("printf '\\33c\\e[3J'")
    logging.basicConfig(filename='session.log', filemode='w', level=logging.INFO)
    print('WELCOME TO THE DECK DATABASE')

    engine = create_engine('sqlite:///deck_database.db', echo = False, future = True)
    db_instance = Deck_DB(engine)

    main_loop = True
    while main_loop == True:

        print('MAIN MENU')
        menu_options = [
            'Print deck list', 
            'Create a new deck', 
            'Modify an existing deck',
            'View and analyse an existing deck',
            'Delete an existing deck',
            'QUIT']
        user_selection = menu_selector(menu_options)

    #PRINT DECKLIST
        if user_selection == 1:
            
            print('DECK LIST')
            decklist = db_instance.fetch_decklist()
            for n in decklist:
                print(n)

    #CREATE NEW DECK
        elif user_selection == 2:
        
            name_status = False
            while name_status == False:    
                deck_name = input('Name your new deck: ')
                name_status = db_instance.validate_deck(deck_name)
            
            db_instance.add_deck(deck_name)

    #MODIFY DECK
        elif user_selection == 3: 

            print('DECK EDITOR')
            decklist = db_instance.fetch_decklist()
            user_selection = menu_selector(decklist)
            deck_inst = Deck_Table(engine, decklist[user_selection - 1])

            sub_loop = True
            while sub_loop == True:

                user_selection = menu_selector([
                    'Print cards in deck',
                    'Add cards',
                    'Remove cards',
                    'Replace cards',
                    'Identify mana rocks',
                    'RETURN TO MAIN MENU'])
            
            #PRINT CARDLIST
                if user_selection == 1:
                    deck_inst.print_header()
                    cardlist = deck_inst.fetch_card_data()
                    for card in cardlist:
                        print(card)

            #ADD CARDS
                elif user_selection == 2:
                    
                    print('CARD ADDER (DO NOT ADD COMMANDER)')

                    card_count = deck_inst.validate_card_count()
                    while card_count == False:

#BUG NOTE: If the deck exceeds the limit the code is not smart enough to make the user remove cards
                        if card_count == False:
                            print('Do you wish to add cards?')
                            user_selection = menu_selector(['Yes', 'No'])

                        if user_selection == 1:
                            
                            card_data = False
                            while card_data == False:
                            
                                card_name = input('What card do you want to add? (Type Q to QUIT): ')
                                if card_name.upper() == 'Q':
                                    break
                                
                                else:
                                    card_data = deck_inst.validate_card_api(card_name)
                                    if card_data == False:
                                        continue 
                                    else:
                                        deck_inst.add_card(card_data)
                                        card_count = deck_inst.validate_card_count()

                        elif user_selection == 2:
                            break

            #REMOVE CARDS
                elif user_selection == 3:

                    print('CARD DELETER')
                    print('Select a card to remove:')
                    cardlist = deck_inst.fetch_card_names()
                    cardlist.append('RETURN TO EDITOR MENU')
                    user_selection = menu_selector(cardlist)

                    if user_selection == len(cardlist):
                        subsub_loop = False
                        continue

                    else:
                        deck_inst.rm_card(cardlist[user_selection - 1])
                        print(f'Removed {cardlist[user_selection - 1]}')

            #REPLACE CARDS
                elif user_selection == 4:
                    
                    print('CARD REPLACER')
                    print('Select a card to replace:')
                    cardlist = deck_inst.fetch_card_names()
                    cardlist.append('RETURN TO EDITOR MENU')
                    user_selection = menu_selector(cardlist)

                    if user_selection == len(cardlist):
                        subsub_loop = False
                        continue

                    else:
                        card_data = False
                        while card_data == False:
                            
                            card_name = input('What card do you want to add? (Type Q to QUIT): ')
                            if card_name.upper() == 'Q':
                                break
                            
                            else:
                                card_data = deck_inst.validate_card_api(card_name)
                                if card_data == False:
                                    continue 
                                else:
                                    deck_inst.add_card(card_data)
                                    card_count = deck_inst.validate_card_count()
                                    deck_inst.replace_card(cardlist[user_selection - 1], card_data)
                                    print(f'Replaced {cardlist[user_selection - 1]} with {card_name}')

            #IDENTIFY MANA ROCKS
                elif user_selection == 5:
                    
                    subsub_loop = True
                    while subsub_loop == True:

#BUG NOTE: due to how statements are printed, when no rocks have been identified, it looks like ALL rocks are identified
                        print('MANA ROCK DESIGNATOR')
                        print('Presently identified rocks are:')
                        deck_inst.print_rocks()

                        cardlist = deck_inst.select_where('Artifact')
                        cardlist.append('RETURN TO EDITOR MENU')
                        user_selection = menu_selector(cardlist)

                        if user_selection == len(cardlist):
                            subsub_loop = False
                            continue

                        else:
                            deck_inst.identify_rock(cardlist[user_selection - 1])

            #RETURN TO MAIN
                elif user_selection == 6:
                    break

    #VIEW AND ANALYSE EXISTING DECK
        elif user_selection == 4:

            decklist = db_instance.fetch_decklist()
            user_selection = menu_selector(decklist)
            deck_inst = Deck_Table(engine, decklist[user_selection - 1])

            sub_loop = True
            while sub_loop == True:

                print('DECK ANALSYER')
                user_selection = menu_selector([
                    'Print cards in deck',
                    'Simulate deck',
                    'RETURN TO MAIN MENU'
                    ])

            #PRINT CARDLIST
                if user_selection == 1:
                    deck_inst.print_header()
                    cardlist = deck_inst.fetch_card_data()
                    for card in cardlist:
                        print(card)

#DEV NOTE: SIMULATION OUTPUTS STILL NEED TO BE RIGOROUSLY TESTED TO VALIDATE THE METHOD
            #SIMULATE DECK
                elif user_selection == 2:
                    
                    validation = False
                    while validation == False:
                        validation = int_validator(input('Input the number of TURNS to simulate: '))
                    sim_turns = validation

                    validation = False
                    while validation == False:
                        validation = int_validator(input('Input the number of GAMES to simulate: '))
                    sim_iterations = validation

                    sim_results = playtest(engine, deck_inst.deck_name, sim_turns, sim_iterations)
                    calc_medians(sim_results)

            #RETURN TO SUB MENU
                elif user_selection == 3:
                    sub_loop = False

    #DELETE EXISTING DECK
        elif user_selection == 5:

            print('DECK DELETER - OPERATION CANNOT BE UNDONE')
            decklist = db_instance.fetch_decklist()
            decklist.append('RETURN TO MAIN MENU')
            user_selection = menu_selector(decklist)
            
            if user_selection == len(decklist):
                continue

            else:
                db_instance.rm_deck(decklist[user_selection-1])

    #QUIT APPLICATION
        elif user_selection == 6:
            main_loop = False