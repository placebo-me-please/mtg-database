# mtg-database

## WHAT THE HECK IS THIS

In its current form, this project consists of a Python script that 1.) manages a SQL database that contains EDH-style decks, 2.) simulates decks to report basic performance metrics to the user, and 3.) serves as a *very* rudimentary menu system. Essentially, the script functions as the back-end to a web application I will (eventually) develop. Consequently, the visual display and menu logic are minimally viable systems.

## THE DATABASE

Deck data is stored persistently via a SQL database file called `deck_database.db`. It comes preloaded with a [Hosts of Mordor](https://magic.wizards.com/en/news/announcements/the-lord-of-the-rings-tales-of-middle-earth-commander-decklists) pre-con deck for the user to experiment with. The user can add their own decks since the database exists locally on their machine. The Python script leverages SQL Alchemy to compile and execute statements necessary to add, remove, and replace cards stored in SQL tables that represent physical decks. Cards are added by sending messages to, and decoding response from, the Scryfall API - **an internet connection is therefore necessary for this to work**.

## SIMULATIONS

The simulation feature is the primary motivator for this project. Following the basic rules of MTG, it 'plays' x turns y times, where x / y are specified by the user. Per turn the script:

- Plays a land if able (i.e. adds to landbase)
- Plays a mana rock if able
- Calculates a running average of CMCs for non-land permanent and non-permanent cards drawn

For each of these metrics, the simulation outputs the median value from the dataset. Essentially, the results allow the user to assess whether their deck can, over the course of a game, afford the cards being drawn. These are simple metrics, but very informative when simulated randomly.

The simulation only reports metrics. It does not make recommendations to the user. It also does not recognize rules that typically enable a player to play multiple lands per turn, tutor for mana rocks, and so on. The user is responsible for understanding the meta of the deck they are simulating.

Simulations are logged to a `session.log` file that can be audited, but I have not yet thoroughly validated the simulation outputs - TAKE ALL RESULTS WITH A GRAIN OF SALT :)

## HOW TO USE

Simply download the `mtg_database.py` script and `deck_database.db` files to start. If you do not download the database file, the script will create the file for you. In a terminal session, `cd` to the directory containing the script, then execute the script:

`$ cd /path/to/script-dir`

`$ python3 mtg_database.py`

## ROADMAP

There are some low-hanging bugs that I need to fix. They are identified with comments in the script itself. Long-term, I will create a Flask application to serve as the front-end for the main script. I will also investigate other interesting metrics to simulate and increase the complexity of the game logic.

## PROJECT STATUS

As of 09 Dec 2023, I will be making minor edits to the main script file to fix bugs, validate the simulation output, and improve the unittest script. I will likely begin working on the Flask app in 2024.
