import json
import mtg_database
import os
import requests
import sqlalchemy
import unittest

from sqlalchemy import MetaData, Table, String
from sqlalchemy import create_engine, select
from unittest.mock import patch

class Build_And_Sort_Tests(unittest.TestCase):

#FUTURE NOTE: build a non-functional deck that includes a variety of cards to exercise the code and detect bugs
#FUTURE NOTE: test does not make any assertions: it is used strictly to debug visual display of information
    #front end implementation will obviate this test

    test_strings = [
        'TEST DECK',
        'Niv-Mizzet, Parun',
        'Steel Hellkite',
        'Ugin, the Spirit Dragon',
        'Sinister Starfish',
        'Ponder',
        'Rite of Replication',
        'Mystic Remora',
        'Izzet Cluestone',
        'Mountain',
        'Island']

    @patch('builtins.input', side_effect = test_strings)
    def test_build_print(self, mock_inputs):

        print('\n')
        print('---TESTING DECK BUILDING AND DISPLAY---')
        engine = create_engine('sqlite:///test_deck_database.db', echo = False)    
        connection_inst = mtg_database.Deck_DB(engine)

        # #TURN ON IF DATABASE OR TABLE ARE DELETED
        # print('creating TABLE named TEST DECK')
        # connection_inst.add_deck()

        print('creating instance of TEST DECK object')
        table_inst = mtg_database.Deck_Table(engine, 'TEST DECK')

        # #TURN ON IF DATABASE OR TABLE IS DELETED
        # print('adding cards to TEST DECK')
        # i = 0
        # while i < 10:
        #     i += 1
        #     table_inst.add_card()

        print('displaying TEST DECK')
        table_inst.print_header()
        cardlist = table_inst.fetch_card_data()
        for card in cardlist:
            print(card)

        print('creating TEST DECK object')
        deck_obj = mtg_database.Deck_Obj(table_inst)

#FUTURE NOTE: build this test case to be ahieve full white box coverage
        print('validating DECK SIZE')
        table_inst.validate_card_count()


class Deck_Table_Operation_Tests(unittest.TestCase):

    test_strings = ['Niv-Mizzet, Parun', 'Niv-Mizzet, Parun']

    @patch('builtins.input', lambda *args: 'TEST DECK')
    def test_validate_card(self):

        print('\n')
        print('---TESTING CARD NAME VALIDATION---')
        engine = create_engine('sqlite://', echo = False)    
        connection_inst = mtg_database.Deck_DB(engine)

        print('creating TABLE named TEST DECK')
        connection_inst.add_deck('TEST DECK')

        print('creating instance of TEST DECK object')
        table_inst = mtg_database.Deck_Table(engine, 'TEST DECK')

        print('verifying NULL CARD entry passes')
        r = table_inst.validate_card_api('null card')
        self.assertEqual(r, False)

#FUTURE NOTE: make this test more robust - it is currently weak
        print('verifying valid card entry passes')
        r = table_inst.validate_card_api('glimmer bairn')
        self.assertEqual(r.status_code, 200)

    @patch('builtins.input', side_effect = test_strings)
    def test_add_rm_replace(self, mock_inputs):

        print('\n')
        print('---TEST BASIC DECK OPERATIONS---')
        engine = create_engine('sqlite://', echo = False)    
        connection_inst = mtg_database.Deck_DB(engine)

        print('creating TABLE named TEST DECK')
        connection_inst.add_deck('TEST DECK')

        print('creating instance of TEST DECK')
        table_inst = mtg_database.Deck_Table(engine, 'TEST DECK')

        print('adding card to TEST DECK')
        card_data = table_inst.validate_card_api('Niv-Mizzet, Parun')
        table_inst.add_card(card_data)

        print('verifying card was added to TEST DECK')
        metadata_obj = MetaData()
        metadata_obj.reflect(engine)
        
        stmnt = select(table_inst.table_obj).where(table_inst.table_obj.c.Name == 'Niv-Mizzet, Parun')
        stmnt.compile()

        card_count = 0
        with engine.connect() as conn:
            for row in conn.execute(stmnt):
                card_count += 1
                card_name = row[0]

            conn.commit()

        self.assertEqual(card_name, 'Niv-Mizzet, Parun')
        self.assertEqual(card_count, 1)

        print('removing card from TEST DECK')
        table_inst.rm_card('Niv-Mizzet, Parun')

        print('verifying card was removed from TEST DECK')
        metadata_obj.clear()
        metadata_obj.reflect(engine)

        stmnt = select(table_inst.table_obj)
        stmnt.compile()

        card_count = 0
        with engine.connect() as conn:
            for row in conn.execute(stmnt):
                card_count += 1
            conn.commit()

        self.assertEqual(card_count, 0)

class Deck_DB_Tests(unittest.TestCase):

    deck_strings = ['TEST DECK', 'TEST DECK']

    @patch('builtins.input', side_effect = deck_strings)
    def test_basic_deck_ops(self, mock_inputs):

        print('\n')
        print('---TESTTING BASIC DATABASE OPERATIONS---')
        engine = create_engine('sqlite://', echo = False)    
        connection_inst = mtg_database.Deck_DB(engine)

        print('creating TABLE named TEST DECK')
        deck_name = 'TEST DECK'
        connection_inst.add_deck(deck_name)

        #kills two birds with one stone: verifies a deck was added whilst invalidating the repeat entry
        print('verifying TEST DECK is used name')
        r = connection_inst.validate_deck(deck_name)
        self.assertEqual(r, False)

        print('removing TABLE named TEST DECK')
        connection_inst.rm_deck(deck_name)

        print('verifying TEST DECK was dropped from DB')
        deck_name = 'TEST DECK'
        r = connection_inst.validate_deck(deck_name)
        self.assertEqual(r, True)

        print('verifying long name validation passes')
        deck_name = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        r = connection_inst.validate_deck(deck_name)
        self.assertEqual(r, False)

        print('verifying correct name validation passes')
        deck_name = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        r = connection_inst.validate_deck(deck_name)
        self.assertEqual(r, True)

if __name__ == '__main__':
    os.system("printf '\\33c\\e[3J'")
    unittest.main(verbosity = 2)